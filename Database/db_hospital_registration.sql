-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 10:26 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hospital_registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE `phonebooks` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `name` varchar(16) NOT NULL,
  `pho_number` int(16) NOT NULL,
  `mob_number` int(16) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonebooks`
--

INSERT INTO `phonebooks` (`id`, `picture`, `name`, `pho_number`, `mob_number`, `email`) VALUES
(1, 'Untitled-1.jpg', 'Sadia Rahman', 27568954, 1816364709, 'sadia@gmail.com'),
(2, 'Untitled-2 copy.jpg', 'Tahmina Aktar', 27568954, 1816364705, 'tahmina27185@gmail.com'),
(3, 'Untitled-7 copy.jpg', 'Tahmina Aktar', 27589569, 1813698156, 'meenaeity@gmail.com'),
(4, 'Untitled-1.jpg', 'Tahmina Aktar', 27568954, 1816364703, 'tahmina27185@gmail.com'),
(6, 'Untitled-8 copy.jpg', 'Md. Shamim Ahmed', 27568954, 1813698156, 'tahmina27185@gmail.com'),
(8, 'Untitled-9 copy.jpg', 'Shamsuddin', 27568954, 1816364705, 'tahshamim@gmail.com'),
(9, 'Untitled-3.jpg', 'Md. Shamim', 27568954, 1813698156, 'tahshamim@gmail.com'),
(10, 'Untitled-2 copy.jpg', 'Mrs.Tahmina', 27589569, 1816364703, 'meenaeity@gmail.com'),
(11, 'Untitled-7 copy.jpg', 'Sisty', 2758964, 1813698156, 'sadia@gmail.com'),
(12, 'Untitled-1.jpg', 'Samira', 27568954, 1816364705, 'samira@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonebooks`
--
ALTER TABLE `phonebooks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonebooks`
--
ALTER TABLE `phonebooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
