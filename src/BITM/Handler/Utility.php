<?php
	namespace App\BITM\Handler;
	
	class Utility{
		
		static public function d($peram = false){
			echo "<pre>";
				var_dump($peram);
			echo "</pre>";
		}

		static public function dd($peram = false){
			self::d($peram);
			die();
		}
		
		
		static public function redirect($url="/Mini-project/Views/BITM/PhoneBook/index.php"){
        header("Location:".$url);
    }
    
    static public function message($message = null){
        if(is_null($message)){ // please give me message
            $_message = self::getMessage();
            return $_message;
        }else{ //please set this message
            self::setMessage($message);
        }
    }
    
    static private function getMessage(){
        
        $_message =  $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_message;
    }
    
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
	}

?>