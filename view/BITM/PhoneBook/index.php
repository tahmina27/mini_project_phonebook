<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Mini-project'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'startup.php');
use \App\BITM\PhoneBook\PhoneBook;
use App\BITM\Handler\Utility;

$phone_obj=new PhoneBook();
    //$titles =$book->getAllTitle(); 


 $search = "";
//Utility::dd($search);
 
  if( strtoupper($_SERVER['REQUEST_METHOD']) == 'GET'){
          $search = isset($_GET['search'])?$_GET:array('search'=>'');
          $phone_objs=$phone_obj->index($search);
    }
	
	
	
//var_dump($search);


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mini Project </title>
	

 <link href="../../../resource/Bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
 <link href="../../../resource/css/styles.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	#message{
       background-color:#66A523;
		}
        </style>
	</head>
	<body class="flaticon bg-login" style="overflow: visible;">
		<div class="container">
			<!-- header start-->
		<header><br>
			<div class="row header">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					<h1>Phone Book</h1>
				</div> 
				<div class="col-md-4 text-right">
					<form class="form-inline" action="index.php" method="GET" >
						<div class="form-group">
							<input type="text" name="search" value="<?php echo $search['search'];?>" /> 
						</div>
						<div class="form-group">
							 <button type="submit" class="btn btn-primary"> Search </button>
						</div>
					</form>
				</div>  				
			</div>
		</header>
			<!-- header End-->
			
			<!-- middle body start-->			
			<section style="display: block;" class="gr_box login" id="gr_loginbox">		
				<div class="box">
						<a href="../../../index.html" button type="button" class="btn btn-primary">Home</button></a>
						<a href="create.php" button type="button" class="btn btn-primary">Add New Contact</button></a>
						<a href="index.php" button type="button" class="btn btn-primary">Contact List</button></a>
					<p class="separator"><span>&nbsp;</span></p>
					<div id="message">
					<?php echo Utility::message();?>
					</div>
					<table class="table table-bordered">
						<thead>
							<tr class="info text-center">
								<th class="text-center">Image </th>
								<th class="text-center">Name </th>
								<th class="text-center">Action </th>
							</tr>
						</thead>
						<tbody>
						<?php
						 if(count($phone_objs) > 0){
						foreach($phone_objs as $list){
						?>
							<tr class="active text-center">
								<td><a href="show.php?id=<?php echo $list['id'];?>"><?php echo "<img src='images/$list[picture]' width='80' height='80' alt='picture here'>"?></a></td>
								<td><a href="show.php?id=<?php echo $list['id'];?>"><?php echo $list['name'];?></a></td>
								<td><a href="edit.php?id=<?php echo $list['id'];?>">Edit</a> 
								| <a href="show.php?id=<?php echo $list['id'];?>">View</a> 
								| <a href="delete.php?id=<?php echo $list['id'];?>" class="delete">Delete</a>
								</td>
								
							</tr>
								<?php
							   
								}
								   }else{
									   ?>
									<tr><td colspan="5"> No record found</td></tr>
									<?php
								   }
								?>
						</tbody>
					</table>
					
					
				</div>
			</section>		
				
			<!-- middle body End-->

				
			<!-- Footer start-->

			<div class="col-md-4">
			</div>
			
			<div class="col-md-8">
				<p>Copyright @ The Code Warriors</p>
			</div>
			<!-- Footer End-->

		</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
		<script>
		  $(function() {
			var availableTags = [
				 '<?php echo implode("','",$titles);?>'   
			];
			$( "#filterTitle" ).autocomplete({
			  source: availableTags
			});
		  });
  </script>
	
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(5000);
        </script>
	
	
  </body>
</html>